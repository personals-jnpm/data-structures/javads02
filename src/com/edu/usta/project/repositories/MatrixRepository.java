package com.edu.usta.project.repositories;

import com.edu.usta.project.models.Field;
import com.edu.usta.project.models.FieldType;

import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MatrixRepository {

    private final Field[][] matrix;
    private final List<Field> fields;

    public MatrixRepository() {
        this.matrix = new Field[3][3];
        fields = new ArrayList<>();
        initMatrix();
    }

    public boolean addField(int position, FieldType fieldType) {
        Field field = fields.get(position - 1);
        if (field.isState()) {
            field.setState(false);
            field.setFieldType(fieldType);
            return true;
        }
        return false;
    }

    public void print() {
        System.out.println(" | " + matrix[0][0] + " | " + matrix[0][1] + " | " + matrix[0][2] + " | ");
        System.out.println(" | " + matrix[1][0] + " | " + matrix[1][1] + " | " + matrix[1][2] + " | ");
        System.out.println(" | " + matrix[2][0] + " | " + matrix[2][1] + " | " + matrix[2][2] + " | ");
    }

    public void initMatrix() {
        int position = 1;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Field field = new Field(position);
                fields.add(field);
                matrix[i][j] = field;
                position++;
            }
        }
    }

    public FieldType compareRows() {
        for (int i = 0; i < 3; i++) {
            if (matrix[i][0].getFieldType() == matrix[i][1].getFieldType() &&
                    matrix[i][0].getFieldType() == matrix[i][2].getFieldType()) {
                return matrix[i][0].getFieldType();
            }
        }
        return FieldType.Empty;
    }

    public FieldType compareColumns() {
        for (int i = 0; i < 3; i++) {
            if (matrix[0][i].getFieldType() == matrix[1][i].getFieldType() &&
                    matrix[0][i].getFieldType() == matrix[2][i].getFieldType()) {
                return matrix[0][i].getFieldType();
            }
        }
        return FieldType.Empty;
    }

    public FieldType compareDiagonals() {
        if (matrix[0][0].getFieldType() == matrix[1][1].getFieldType() &&
                matrix[0][0].getFieldType() == matrix[2][2].getFieldType()) {
            return matrix[0][0].getFieldType();
        }
        if (matrix[0][2].getFieldType() == matrix[1][1].getFieldType() &&
                matrix[0][2].getFieldType() == matrix[2][0].getFieldType()) {
            return matrix[0][2].getFieldType();
        }
        return FieldType.Empty;
    }

}
