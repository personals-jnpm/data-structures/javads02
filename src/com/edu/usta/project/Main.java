package com.edu.usta.project;

import com.edu.usta.project.controllers.MatrixController;
import com.edu.usta.project.models.FieldType;

import java.util.Scanner;

public class Main {

    static Scanner keyboard = new Scanner(System.in);
    static final MatrixController matrix = new MatrixController();
    static final int PLAYER_ONE = 1;
    static final int PLAYER_TWO = 2;

    public static void main(String[] args) {
        int player = PLAYER_ONE;
        do {
            try {
                addField(getFieldType(player), player);
                matrix.printMatrix();
                player = player == PLAYER_ONE ? PLAYER_TWO : PLAYER_ONE;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (!matrix.isGameOver());
        System.out.println("¡EL juego ha finalizado!");
        System.out.println(getWinner());
    }

    public static void addField(FieldType fieldType, int player) throws Exception {
        int position;
        do {
            System.out.print("Turno jugador " + player + ", ingrese la posición que desea jugar: ");
            position = readInteger();
            validatePosition(position);
        } while (!matrix.addField(position, fieldType));
    }

    public static FieldType getFieldType(int option) {
        FieldType type = FieldType.Empty;
        switch (option) {
            case PLAYER_ONE -> type = FieldType.O;
            case PLAYER_TWO -> type = FieldType.X;
            default -> System.out.println("¡Opción no valida!");
        }
        return type;
    }

    public static String getWinner() {
        String result = "";
        switch (matrix.getWinner()) {
            case O -> result = "El ganador es el jugador número " + PLAYER_ONE;
            case X -> result = "El ganador es el jugador número " + PLAYER_TWO;
            case Empty -> result = "¡Ha ocurrido un empate!";
            default -> System.out.println("¡Ocurrió un problema al validar el ganador!");
        }
        return result;
    }

    public static void validatePosition(int position) throws Exception {
        if (position <= 0 || position > 9) {
            throw new Exception("¡La posición ingresada no es válida!");
        }
    }

    private static int readInteger() {
        String numberStr = keyboard.nextLine();
        int number = 0;
        if (numberStr.matches("[0-9]+")) {
            number = Integer.parseInt(numberStr);
        }
        return number;
    }
}
