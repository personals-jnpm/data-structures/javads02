package com.edu.usta.project.models;

public class Field {

    private boolean state;
    private FieldType fieldType;
    private int number;

    public Field() {
    }

    public Field(int number) {
        this.number = number;
        state = true;
        fieldType = FieldType.Empty;
    }

    public Field(boolean state, FieldType fieldType, int number) {
        this.state = state;
        this.fieldType = fieldType;
        this.number = number;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        if (state) {
            return "" + number;
        }
        return "" + fieldType;
    }
}
