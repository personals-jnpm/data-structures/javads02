package com.edu.usta.project.controllers;

import com.edu.usta.project.models.FieldType;
import com.edu.usta.project.repositories.MatrixRepository;

public class MatrixController {

    private FieldType playerWinner = FieldType.Empty;
    private int attempts;
    private final MatrixRepository repository;

    public MatrixController() {
        this.repository = new MatrixRepository();
        this.attempts = 0;
    }

    public int getAttempts() {
        return attempts;
    }

    public boolean addField(int position, FieldType fieldType) {
        boolean addField = repository.addField(position, fieldType);
        if (addField) {
            attempts++;
        }
        return addField;
    }

    public void printMatrix() {
        repository.print();
    }

    public boolean isGameOver() {
        playerWinner = repository.compareRows();
        if (playerWinner != FieldType.Empty) {
            return true;
        }
        playerWinner = repository.compareColumns();
        if (playerWinner != FieldType.Empty) {
            return true;
        }
        if (attempts >= 9) {
            playerWinner = FieldType.Empty;
            return true;
        }
        playerWinner = repository.compareDiagonals();
        return playerWinner != FieldType.Empty;
    }

    public FieldType getWinner() {
        return playerWinner;
    }

}
